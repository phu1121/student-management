package quanly.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import quanly.model.entity.SinhvienEntity;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<SinhvienEntity, String> {
    List<SinhvienEntity> findByHoten(String hoTen);//Hoten là tên cột, từ khóa findBy là của jpa
    //làm ví dụ tương tự tìm kiếm dựa trên nhiều thông tin nhé
    SinhvienEntity findByHotenAndGioitinhAndTrangthai(String hoTen, String gioiTinh, String tranThai);
        //Lưu ý tham số truyề vào cũng phải theo thứ tự
        //Hoten -> String
    //Hiểu cách dùng cái jpa này chưa
//    findbySinhvienEntityhoten //cái ni sai là do bảng sinhvien ko có cột nào tên là "SinhvienEntityhoten"
}

