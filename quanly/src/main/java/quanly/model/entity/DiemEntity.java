package quanly.model.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "DIEM")
public class DiemEntity {
    private Double toan;
    private Double li;
    private Double hoa;
    private Double diemtb;
    private String trangthai;
    private String id;

    @Basic
    @Column(name = "TOAN")
    public Double getToan() {
        return toan;
    }

    public void setToan(Double toan) {
        this.toan = toan;
    }

    @Basic
    @Column(name = "LI")
    public Double getLi() {
        return li;
    }

    public void setLi(Double li) {
        this.li = li;
    }

    @Basic
    @Column(name = "HOA")
    public Double getHoa() {
        return hoa;
    }

    public void setHoa(Double hoa) {
        this.hoa = hoa;
    }

    @Basic
    @Column(name = "DIEMTB")
    public Double getDiemtb() {
        return diemtb;
    }

    public void setDiemtb(Double diemtb) {
        this.diemtb = diemtb;
    }

    @Basic
    @Column(name = "TRANGTHAI")
    public String getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(String trangthai) {
        this.trangthai = trangthai;
    }

    @Id
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiemEntity that = (DiemEntity) o;
        return Objects.equals(toan, that.toan) &&
                Objects.equals(li, that.li) &&
                Objects.equals(hoa, that.hoa) &&
                Objects.equals(diemtb, that.diemtb) &&
                Objects.equals(trangthai, that.trangthai) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(toan, li, hoa, diemtb, trangthai, id);
    }
}
