package quanly.model.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "GIANGVIEN")
public class GiangvienEntity {
    private String id;
    private String hoten;
    private Timestamp ngaysinh;
    private String quequan;
    private Boolean gioitinh;
    private String trangthai;
    private Timestamp ngaytao;
    private Timestamp ngaycn;

    @Id
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "HOTEN")
    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    @Basic
    @Column(name = "NGAYSINH")
    public Timestamp getNgaysinh() {
        return ngaysinh;
    }

    public void setNgaysinh(Timestamp ngaysinh) {
        this.ngaysinh = ngaysinh;
    }

    @Basic
    @Column(name = "QUEQUAN")
    public String getQuequan() {
        return quequan;
    }

    public void setQuequan(String quequan) {
        this.quequan = quequan;
    }

    @Basic
    @Column(name = "GIOITINH")
    public Boolean getGioitinh() {
        return gioitinh;
    }

    public void setGioitinh(Boolean gioitinh) {
        this.gioitinh = gioitinh;
    }

    @Basic
    @Column(name = "TRANGTHAI")
    public String getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(String trangthai) {
        this.trangthai = trangthai;
    }

    @Basic
    @Column(name = "NGAYTAO")
    public Timestamp getNgaytao() {
        return ngaytao;
    }

    public void setNgaytao(Timestamp ngaytao) {
        this.ngaytao = ngaytao;
    }

    @Basic
    @Column(name = "NGAYCN")
    public Timestamp getNgaycn() {
        return ngaycn;
    }

    public void setNgaycn(Timestamp ngaycn) {
        this.ngaycn = ngaycn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GiangvienEntity that = (GiangvienEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(hoten, that.hoten) &&
                Objects.equals(ngaysinh, that.ngaysinh) &&
                Objects.equals(quequan, that.quequan) &&
                Objects.equals(gioitinh, that.gioitinh) &&
                Objects.equals(trangthai, that.trangthai) &&
                Objects.equals(ngaytao, that.ngaytao) &&
                Objects.equals(ngaycn, that.ngaycn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, hoten, ngaysinh, quequan, gioitinh, trangthai, ngaytao, ngaycn);
    }
}
