package quanly.util;

public class StringUtil {
    public static String uppercase(String input) {
        if (input == null) {
            return null;
        }

        return input.toUpperCase();
    }
}
