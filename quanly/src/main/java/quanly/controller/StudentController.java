package quanly.controller;


import com.sun.istack.Nullable;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import quanly.model.entity.GiangvienEntity;
import quanly.model.entity.ReponseObject;
import quanly.model.entity.SinhvienEntity;
import quanly.repository.StudentRepository;
import quanly.validator.SinhVienValidator;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RestController
@RequestMapping("/api/v1/students")
public class StudentController {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private StudentRepository studentRepository;

    SinhVienValidator validator = new SinhVienValidator();

    @RequestMapping(
            name = "/",
            method = RequestMethod.GET
    )
    public List<SinhvienEntity> getAllStudent(){
            return studentRepository.findAll();
    }

    @PutMapping("/{id}")
    ResponseEntity<ReponseObject> finById(@PathVariable String id) {
        //Mặc định response là false
        ReponseObject response = new ReponseObject("false", "cannot find stuudent with id =" + id, "");
        Optional<SinhvienEntity> foundStudent = studentRepository.findById(id);

        //Chỉ khi tìm đc data thì chỉnh sửa response thành oke
        if (foundStudent.isPresent()) {
            response.setStatus("OK");
            response.setMessage("query sucsessfully student");
            response.setData(foundStudent.get());
        }

        //nên câu lệnh trả về có thể ngắn gọn lại ntn,
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PostMapping("/insert")
    ResponseEntity<SinhvienEntity> insertStudent(@RequestBody SinhvienEntity student) {
        validator.validateFormatCreate(student);

        try {
            student.setId(UUID.randomUUID().toString());
            student.setNgaytao(new Timestamp(System.currentTimeMillis()));
            student.setNgaycn(new Timestamp(System.currentTimeMillis()));
            student.setTrangthai("ACTIVE");
            studentRepository.save(student);
            return ResponseEntity.ok(student);
        } catch (Exception e) {
            logger.error("Create new SinhvienEntity Error", e);
            //Todo - Custom Exception
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PutMapping(value = "/put/{id}")
    ResponseEntity<SinhvienEntity> updateStudent (@RequestBody SinhvienEntity student, @PathVariable String id){
        Optional<SinhvienEntity> optionalSinhvienEntity = studentRepository.findById(id);
        if (!optionalSinhvienEntity.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        try {
            SinhvienEntity entity = optionalSinhvienEntity.get();
            entity.setHoten(student.getHoten());
            entity.setTrangthai(student.getTrangthai());
            entity.setGioitinh(student.getGioitinh());
            entity.setQuequan(student.getQuequan());
            entity.setNgaysinh(student.getNgaysinh());
            entity.setNgaycn(new Timestamp(System.currentTimeMillis()));

            studentRepository.save(entity);
            return ResponseEntity.ok(entity);
        } catch (Exception e) {
            logger.error("Update SinhvienEntity Error", e);
            //Todo - Custom Exception
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
    @DeleteMapping("/{id}")
    ResponseEntity<ReponseObject> deleteStudent( @PathVariable String id){
        boolean exits = studentRepository.existsById(id);
        if (exits){
            studentRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ReponseObject( "ok ", "delete student successfully","")
            );
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ReponseObject("fail","no matching id found","")
        );
    }

}
